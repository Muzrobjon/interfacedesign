import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(name = "RegisterServlet", urlPatterns = {"/register"})
public class RegisterServlet extends HttpServlet {
    private static final String INSERT_QUERY = "INSERT INTO bookdata (BOOKNAME, BOOKEDITION, BOOKPRICE) VALUES (?, ?, ?)";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        PrintWriter pw = res.getWriter();
        res.setContentType("text/html");
        String BOOKNAME = req.getParameter("BOOKNAME");
        String BOOKEDITION = req.getParameter("BOOKEDITION");
        float BOOKPRICE = Float.parseFloat(req.getParameter("BOOKPRICE"));

        try {
            // Load JDBC driver (consider using try-with-resources for better resource management)
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        }

        try (Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/book", "root", "@Designer1980");
             PreparedStatement ps = con.prepareStatement(INSERT_QUERY)) {

            ps.setString(1, BOOKNAME);
            ps.setString(2, BOOKEDITION);
            ps.setFloat(3, BOOKPRICE);

            int count = ps.executeUpdate();

            if (count == 1) {
                pw.println("<h2> Record is inserted successfully</h2>");
            } else {
                pw.println("<h2> Record insertion failed</h2>");
            }

        } catch (SQLException se) {
            se.printStackTrace();
            pw.println("<h1>" + se.getMessage() + "</h1>");
        } catch (Exception e) {
            e.printStackTrace();
            pw.println("<h1>" + e.getMessage() + "</h1>");
        }
        pw.println("<a href='index.jsp'>HOME</a>");
        pw.println("<br>");
        pw.println("<a href='bookList'>BookList</a>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }
}
