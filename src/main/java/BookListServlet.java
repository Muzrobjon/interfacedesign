import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "BookListServlet", urlPatterns = {"/bookList"})
public class BookListServlet extends HttpServlet {

    private static final String QUERY = "select * from bookdata";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        PrintWriter pw = res.getWriter();
        res.setContentType("text/html");

        try {
            // Load JDBC driver (consider using try-with-resources for better resource management)
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        }

        try (Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/book", "root", "@Designer1980");
             PreparedStatement ps = con.prepareStatement(QUERY)) {
            ResultSet rs = ps.executeQuery();

            pw.println("<table border ='1' align = center>");
            pw.println("<tr>");
            pw.println("<th>BOOK ID</th>");
            pw.println("<th>BOOK Name</th>");
            pw.println("<th>Book edition</th>");
            pw.println("<th>Book price</th>");
            pw.println("<th>Edit</th>");
            pw.println("<th>Delete</th>");
            pw.println("</tr>");

            while (rs.next()) {
                pw.println("<tr>");
                pw.println("<td>" + rs.getInt(1) + "</td>");
                pw.println("<td>" + rs.getString(2) + "</td>");
                pw.println("<td>" + rs.getString(3) + "</td>");
                pw.println("<td>" + rs.getFloat(4) + "</td>");
                pw.println("<td> <a href =editScreen?id="+rs.getInt(1)+"'>Edit</a> </td>");
                pw.println("<td> <a href =deleteurl?id="+rs.getInt(1)+"'>Delete</a> </td>");
                pw.println("</tr>");
            }

            pw.println("</table>");

        } catch (SQLException se) {
            se.printStackTrace();
            pw.println("<h1>" + se.getMessage() + "</h1>");
        } catch (Exception e) {
            e.printStackTrace();
            pw.println("<h1>" + e.getMessage() + "</h1>");
        }
        pw.println("<a href='index.jsp'>HOME</a>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }
}
