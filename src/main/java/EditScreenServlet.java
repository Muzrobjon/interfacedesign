import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet("/editScreen")
public class EditScreenServlet extends HttpServlet {
    private static final String QUERY = "SELECT BOOKNAME, BOOKEDITION, BOOKPRICE from bookdata where id=? ";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        PrintWriter pw = res.getWriter();
        res.setContentType("text/html");

        String idParameter = req.getParameter("id");

        // Clean the "id" parameter from non-numeric characters
        String cleanedId = idParameter.replaceAll("[^0-9]", "");

        if (!cleanedId.isEmpty()) {
            int id = Integer.parseInt(cleanedId);

            try {
                // Load JDBC driver (consider using try-with-resources for better resource management)
                Class.forName("com.mysql.cj.jdbc.Driver");
            } catch (ClassNotFoundException cnf) {
                cnf.printStackTrace();
            }

            try (Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/book", "root", "@Designer1980");
                 PreparedStatement ps = con.prepareStatement(QUERY)) {

                ps.setInt(1, id);
                ResultSet rs = ps.executeQuery();
                rs.next();

                pw.println("<form action='editurl?id=" + id + "' method='post'>");
                pw.println("<table align='center'>");
                pw.println("<tr>");
                pw.println("<td>BOOK NAME</td>");
                pw.println("<td><input type='text' name='BOOKNAME' value='" + rs.getString(1) + "'></td>");
                pw.println("</tr>");
                pw.println("<tr>");
                pw.println("<td>BOOK EDITION</td>");
                pw.println("<td><input type='text' name='BOOKEDITION' value='" + rs.getString(2) + "'></td>");
                pw.println("</tr>");
                pw.println("<tr>");
                pw.println("<td>BOOK PRICE</td>");
                pw.println("<td><input type='text' name='BOOKPRICE' value='" + rs.getFloat(3) + "'></td>");
                pw.println("</tr>");
                pw.println("<tr>");
                pw.println("<td><input type='submit' value='Edit'></td>");
                pw.println("<td><input type='reset' value='Cancel'></td>");
                pw.println("</tr>");
                pw.println("</table>");
                pw.println("</form>");

            } catch (SQLException se) {
                se.printStackTrace();
                pw.println("<h1>" + se.getMessage() + "</h1>");
            } catch (Exception e) {
                e.printStackTrace();
                pw.println("<h1>" + e.getMessage() + "</h1>");
            }

        } else {
            // Handle the case where "id" is not a valid integer
            pw.println("<h1>Invalid 'id' parameter: " + idParameter + "</h1>");
        }

        pw.println("<a href='index.jsp'>HOME</a>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }
}
