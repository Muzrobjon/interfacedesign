import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet("/editurl")
public class EditServlet extends HttpServlet {
    private static final String QUERY = "UPDATE bookdata SET BOOKNAME=?, BOOKEDITION=?, BOOKPRICE=? WHERE id=? ";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        PrintWriter pw = res.getWriter();
        res.setContentType("text/html");

        String idParameter = req.getParameter("id");
        String BOOKNAME = req.getParameter("BOOKNAME");
        String BOOKEDITION = req.getParameter("BOOKEDITION");
        String BOOKPRICE = req.getParameter("BOOKPRICE");

        if (idParameter != null && idParameter.matches("\\d+")) {
            int id = Integer.parseInt(idParameter);

            try (Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/book", "root", "@Designer1980");
                 PreparedStatement ps = con.prepareStatement(QUERY)) {

                ps.setString(1, BOOKNAME);
                ps.setString(2, BOOKEDITION);
                ps.setFloat(3, Float.parseFloat(BOOKPRICE));
                ps.setInt(4, id);

                int count = ps.executeUpdate();

                if (count == 1) {
                    pw.println("<h2> Record is updated successfully</h2>");
                } else {
                    pw.println("<h2> Record update failed</h2>");
                }

            } catch (SQLException se) {
                se.printStackTrace();
                pw.println("<h1>" + se.getMessage() + "</h1>");
            } catch (Exception e) {
                e.printStackTrace();
                pw.println("<h1>" + e.getMessage() + "</h1>");
            }

        } else {
            // Handle the case where "id" is not a valid integer
            pw.println("<h1>Invalid 'id' parameter: " + idParameter + "</h1>");
        }

        pw.println("<a href='index.jsp'>HOME</a>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }
}
